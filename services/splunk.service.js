
// Reference: https://github.com/splunk/splunk-sdk-javascript/blob/master/examples/node/helloworld/search_oneshot.js
// This example will login to Splunk, perform a oneshot search, and then print 
// out the raw results and some key-value pairs. A one search is one that 
// won't return until the search is complete and return all the search
// results in the response.

const splunkjs = require('splunk-sdk');
const config = require('../config/config.json');
var Async  = splunkjs.Async;

function getSplunkResults(search) {
    return doSplunkSearch(search, config, (err) => console.log(err));
}

async function doSplunkSearch(search, opts, callback) {
    opts = opts || {};
    
    var username = opts.username    || "swyer";
    var password = opts.password    || "adminadmin";
    var scheme   = opts.scheme      || "https";
    var host     = opts.host        || "localhost";
    var port     = opts.port        || "8089";          
    var version  = opts.version     || "default";
    
    var service = new splunkjs.Service({
        username: username,
        password: password,
        scheme: scheme,
        host: host,
        port: port,
        version: version
    });

    return new Promise(resolve => {
        Async.chain([
            // First, we log in
            function(done) {
                service.login(done);
            },
            // Perform the search
            function(success, done) {
                if (!success) {
                    done("Error logging in");
                }
                
                service.oneshotSearch(`search ${search}`, {}, done);
            },
            // The job is done, and the results are returned as a 2D array
            function(results, done) {   
                results.rows.splice(0, 0, results.fields);
                // console.log(results.rows)
                resolve(results.rows);
                // done();
            }
            ],
            function(err) {
                callback(err);        
            }
        );
    });
};

module.exports = {
    getSplunkResults
};
