const splunkService = require('../services/splunk.service');

function transform(item) {
    if (!isNaN(item)) {
        return Number(item);
    } else {
        return item;
    }
}

exports.search = (req, res, next) => {
    const { body } = req;
    splunkService.getSplunkResults(body.query).then(result => {
        const data = result.map(items => {
            return items.map((x) => transform(x));
        });
        console.log(data);
        res.json(data);
    })
};