var express = require('express');
var router = express.Router();
const controller = require('../controllers/splunk.controller');

/* GET home page.s */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/search', controller.search);

module.exports = router;
